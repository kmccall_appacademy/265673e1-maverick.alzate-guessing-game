# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  # display game intro
  start_message

  # main game loop
  mystery_number = Random.rand(99) + 1
  guess_count = 0
  game_over = false
  until game_over
    write(guess_count)
    print "Tell me your guess:"
    # validate input
    guess = gets.chomp.to_i
    # print player guess
    puts "#{guess}"
    # compare guess to actual number & check if game over
    check = compare(guess, mystery_number)
    if check == -1
      puts "too low"
    elsif check == 0
      game_over = true
    elsif check == 1
      puts "too high"
    end
    # increment guess count
    guess_count += 1
  end
  puts "You guessed #{mystery_number} correctly with #{guess_count}"
end

def compare(guess, mystery_number)
  guess <=> mystery_number
end


def write(guess_count)
  puts "You have guessed #{guess_count} times. " if guess_count > 0
end

def valid?(input)
  input.to_i < 1 || input.to_i > 100 ? false : true
end

def start_message
  puts "guess a number"
end




# File shuffler

def file_shuffler
  puts "Please give me a file name."
  file_name = gets.chomp
  lines = File.readlines(file_name.to_s)
  lines.shuffle!
  File.open("#{file_name}-shuffled.txt", "w") do |file_line|
    lines.each { |shuffled_line| puts shuffled_line.to_s }
  end
end
